<?php

namespace App\Tests;

use App\LineReader\FileSplitter;
use PHPUnit\Framework\TestCase;

class FileSplitterTest extends TestCase
{

    /**
     * @dataProvider file
     */
    public function testSplit(string $filePath, int $linesPerChunk, int $linesNumber)
    {
        $fileSplitter = new FileSplitter($filePath);
        $splittedFile = $fileSplitter->split();

        $this->assertEquals($linesPerChunk, $splittedFile->getLinesPerChunk());
        $this->assertEquals($linesNumber, $splittedFile->getOriginalFileLinesNumber());
        $this->assertEquals($filePath, $splittedFile->getOriginalFilePath());
        $this->assertIsString($splittedFile->getFileName());
        $this->assertStringContainsString('lineReader_', $splittedFile->getTmpDirPath());

    }

    public function file(): array
    {
        return [
            [
                'tests/_data/testfile',
                10,
                100
            ]
        ];
    }
}
