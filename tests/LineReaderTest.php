<?php

namespace App\Tests;

use App\LineReader\LineReader;
use PHPUnit\Framework\TestCase;

class LineReaderTest extends TestCase
{
    /**
     * @dataProvider file
     */
    public function testGetLinesNum(string $filePath, int $linesNumber)
    {
        $lineReader = new LineReader($filePath);
        $lines = $lineReader->getLinesNum();

        $this->assertEquals($linesNumber, $lines);
    }

    /**
     * @dataProvider file
     */
    public function testReadLine(string $filePath, int $linesNumber)
    {
        $lineReader = new LineReader($filePath);

        $lineToRead = rand(0, $linesNumber-1);
        $lineLineReader = $lineReader->readLine($lineToRead);

        $file = new \SplFileObject($filePath);
        $file->seek($lineToRead);
        $lineSplObjectFile = $file->current();

        $this->assertEquals($lineSplObjectFile, $lineLineReader);
    }


    public function file(): array
    {
        return [
            [
                'tests/_data/testfile',
                100
            ]
        ];
    }
}
