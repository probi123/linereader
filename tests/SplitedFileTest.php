<?php

namespace App\Tests;

use App\LineReader\SplittedFile;
use PHPUnit\Framework\TestCase;

class SplitedFileTest extends TestCase
{
    /**
     * @dataProvider file
     */
    public function testGetOriginalFileLinesNumber(string $filepath, int $lines): void
    {
        $splittedFile = new SplittedFile();

        $splittedFile->setOriginalFilePath($filepath);
        $splittedFile->setTmpDirPath(sys_get_temp_dir(). '/lineReader_');

        $this->assertEquals($lines, $splittedFile->getOriginalFileLinesNumber());
    }


    public function file(): array
    {
        return [
            [
                'tests/_data/testfile',
                100
            ]
        ];
    }
}
