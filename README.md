
# Line Reader class
## Requirements
To use this class host system needs to have gnu split command (https://www.gnu.org/software/coreutils/manual/html_node/split-invocation.html). Also PHP 8 is required.
## Usage
You can use command to generate random big file:
``` php bin/console generate-random-big-file <number-of-lines  ```

To test performance of this solution you can use test command:
``` php bin/console test-read-line-class <path-to-test-file>  ```

## Performance
Using SplFileObject:
* Random read 100 lines of 10k lines file (~100MB) : 1.7s
* Average random time to read 1 lines of 10k lines file: 0.017s

Using LineReader class:
* Random read 100 lines of 10k lines file (~100MB) : 0.0078s
* Average random time to read 1 lines of 10k lines file: 0.000078s
