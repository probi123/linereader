<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'generate-random-big-file',
    description: 'Add a short description for your command',
)]
class GenerateRandomBigFileCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->addArgument('numberlines', InputArgument::REQUIRED, 'number of line to generate')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $file = fopen("testfile","a");

        $linesNumber = $input->getArgument('numberlines');

        $progressBar = new ProgressBar($output, $linesNumber);
        for($i = 0; $i < $linesNumber; $i++) {
            fwrite($file, $this->generateRandomString() . PHP_EOL);
            $progressBar->advance();
        }

        $progressBar->finish();

        fclose($file);
        $io->success('testfile generated');

        return Command::SUCCESS;
    }

    private function generateRandomString(int $minLength = 5, int $maxLength = 20313): string
    {
        $length = rand($minLength, $maxLength);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
