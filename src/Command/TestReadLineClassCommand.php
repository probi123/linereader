<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\LineReader\LineReader;

#[AsCommand(
    name: 'test-read-line-class',
    description: 'Add a short description for your command',
)]
class TestReadLineClassCommand extends Command
{
    public function __construct() {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('filepath', InputArgument::REQUIRED, 'Test file path')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $filePath = $input->getArgument('filepath');

        $lineReader = new LineReader($filePath);
        $file = new \SplFileObject('testfile');

        $lines = $lineReader->getLinesNum();

        $lineReaderTime = 0;
        $splTime = 0;

        for($i=0; $i < 100; $i++) {
            $lineToRead = rand(0, $lines-1);

            $startTime = microtime(true);
            $lineReaderLine = $lineReader->readLine($lineToRead);
            $endTime = microtime(true);

            $lineReaderTime += $endTime - $startTime ;

            $startTime = microtime(true);
            $file->seek($lineToRead);
            $SplLine = $file->current();
            $endTime = microtime(true);

            $splTime += $endTime - $startTime ;
        }

        $io->note('Line reader time to random read 100 lines: ' . $lineReaderTime );
        $io->note('Line reader average time to random read 100 lines: ' . number_format($lineReaderTime/100, 10));

        $io->note('SplFileObject time to random read 100 lines: ' . $splTime );
        $io->note('SplFileObject average time to random read 100 lines: ' . number_format($splTime/100, 10));
        return Command::SUCCESS;
    }
}
