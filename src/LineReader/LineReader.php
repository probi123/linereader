<?php

namespace App\LineReader;

class LineReader
{
    private SplittedFile $splitedFile;

    public function __construct(
        private string $filePath
    ) {
        $fileSplitter = new FileSplitter($this->filePath);
        $this->splitedFile = $fileSplitter->split();
    }

    public function getLinesNum(): int {
        return $this->splitedFile->getOriginalFileLinesNumber();
    }

    public function readLine(int $lineNumber): string {
        return $this->splitedFile->getLine($lineNumber);
    }
}