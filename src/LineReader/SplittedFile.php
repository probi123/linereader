<?php

namespace App\LineReader;

use App\LineReader\Exception\LineNotExistException;
use Symfony\Component\Filesystem\Filesystem;

class SplittedFile
{
    protected string $tmpDirPath = '';
    protected string $fileName;
    protected int $linesPerChunk;
    protected string $originalFilePath;
    protected int $originalFileLinesNumber = 0;

    public function __construct()
    {
        $this->fileName = sha1(random_int(0, 9999999));
    }

    public function __destruct()
    {
        $fileSystem = new Filesystem();
        if ($fileSystem->exists($this->getTmpDirPath())) {
            $fileSystem->remove($this->getTmpDirPath());
        }
    }

    public function getTmpDirPath(): string
    {
        return $this->tmpDirPath;
    }

    public function setTmpDirPath(string $tmpDirPath): self
    {
        $this->tmpDirPath = $tmpDirPath;
        return $this;

    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;
        return $this;

    }

    public function getPath(): string
    {
        return $this->getTmpDirPath() . '/' . $this->getFileName();
    }

    public function getLinesPerChunk(): int
    {
        return $this->linesPerChunk;
    }

    public function setLinesPerChunk(int $linesPerChunk): self
    {
        $this->linesPerChunk = $linesPerChunk;
        return $this;

    }

    public function getOriginalFilePath(): string
    {
        return $this->originalFilePath;
    }

    public function setOriginalFilePath(string $originalFilePath): self
    {
        $this->originalFilePath = $originalFilePath;
        return $this;

    }

    public function getOriginalFileLinesNumber(): int
    {
        if ($this->originalFileLinesNumber === 0) {
            $file = new \SplFileObject($this->originalFilePath);
            $file->seek(PHP_INT_MAX);

            $this->originalFileLinesNumber = $file->key();
        }

        return $this->originalFileLinesNumber;
    }

    public function getLine(int $lineNumber): string
    {
        $filePath = $this->getFilePathByLineNumber($lineNumber);
        $lineNumberSplitedFile = $this->getSplittedFileLineNumber($lineNumber);

        $file = new \SplFileObject($filePath);
        $file->seek($lineNumberSplitedFile);

        return $file->current();
    }

    protected function getFilePathByLineNumber(int $lineNumber): string
    {
        if ($lineNumber >= $this->getOriginalFileLinesNumber()) {
            throw new LineNotExistException();
        }

        $fileNumber = $this->getFileNumber($lineNumber);

        return $this->getFilePathByFileNumber($fileNumber);
    }

    protected function getFileNumber(int $lineNumber): int
    {
        return floor($lineNumber / $this->getLinesPerChunk());
    }

    protected function getFilePathByFileNumber(int $fileNumber): string
    {
        return $this->getPath() . str_pad($fileNumber, 10, '0', STR_PAD_LEFT );
    }

    protected function getSplittedFileLineNumber(int $originalFileLineNumber): int
    {
        $fileNumber = $this->getFileNumber($originalFileLineNumber);

        $fileStartingLine = $fileNumber * $this->getLinesPerChunk();

        return $originalFileLineNumber - $fileStartingLine;
    }

}