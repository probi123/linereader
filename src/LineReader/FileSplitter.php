<?php

namespace App\LineReader;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;

class FileSplitter
{

    private Filesystem $fileSystem;

    public function __construct(
        protected string $filepath,
    ) {
        $this->fileSystem = new Filesystem();
    }


    public function split(int $linesPerChunk = 10): SplittedFile
    {
        if ($this->fileSystem->exists($this->filepath)) {
            $splittedFile = new SplittedFile();

            $splittedFile->setLinesPerChunk($linesPerChunk)
                        ->setOriginalFilePath($this->filepath)
                        ->setTmpDirPath($this->generateTempDirPath());

            $this->fileSystem->mkdir($splittedFile->getTmpDirPath());

            $this->fileSystem->copy($this->filepath, $splittedFile->getPath(), true);

            $this->splitFile($splittedFile);

            return $splittedFile;
        } else {
            throw new FileNotFoundException(sprintf('File %s doesn\'t expist', $this->filepath));
        }
    }

    protected function generateTempDirPath(): string
    {
        return sys_get_temp_dir() . '/' . 'lineReader_'. random_int(9999, 999999);
    }

    protected function splitFile(SplittedFile $file): void
    {
        exec(sprintf(
            'split -l%d -a 10 -d %s %s',
            $file->getLinesPerChunk(), $file->getPath(), $file->getPath()));
    }
}